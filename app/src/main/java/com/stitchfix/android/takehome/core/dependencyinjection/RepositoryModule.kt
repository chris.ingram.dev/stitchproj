package com.stitchfix.android.takehome.core.dependencyinjection

import com.stitchfix.android.takehome.network.api.CheckoutRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun providesCheckoutRepository(impl: CheckoutRepositoryImpl): CheckoutRepository
}