package com.stitchfix.android.takehome.network.api

data class APIResult <T> (var success: T? = null, var error: String? = null)
