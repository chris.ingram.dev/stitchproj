package com.stitchfix.android.takehome.ui.checkout.adapter

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stitchfix.android.takehome.databinding.ListItemProductBinding
import com.stitchfix.android.takehome.network.data.Product
import com.stitchfix.android.takehome.ui.checkout.holder.ProductHolder


class ProductAdapter(var productSelectListener: ProductHolder.OnProductSelectListener) : RecyclerView.Adapter<ProductHolder>(), ProductHolder.OnDiscard {

    var productList: ArrayList<Product> = arrayListOf()
    var deletedProducts: ArrayList<Product> = arrayListOf()
    var selectedItems: SparseBooleanArray = SparseBooleanArray()
    private var isFinalized = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        return ProductHolder(
            ListItemProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), this, productSelectListener, isFinalized
        )
    }

    private fun toggleSelection(position: Int) {
        if (selectedItems.get(position, false)) {
            selectedItems.delete(position)
            deletedProducts.remove(productList[position])
        } else {
            selectedItems.put(position, true)
            deletedProducts.add(productList[position])
        }
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.bind(productList[position], position, selectedItems[position])
    }

    override fun getItemCount() = productList.size

    override fun discard(product: Product, position: Int) {
        toggleSelection(position)
    }

    fun setFinalize(value: Boolean) {
        isFinalized = value
    }

}