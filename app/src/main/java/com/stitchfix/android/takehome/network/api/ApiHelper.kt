package com.stitchfix.android.takehome.network.api

import com.stitchfix.android.takehome.network.data.BoxResponse
import okhttp3.Response

interface ApiHelper {
    suspend fun currentFix(): BoxResponse
}