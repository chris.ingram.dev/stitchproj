package com.stitchfix.android.takehome.network.data

import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class Product(
    val id: Long,
    val name: String,
    val price: String,
    val brand: String,
    val image_url: String,
    val size: String
) {
    fun getPrice(): BigDecimal {
        return BigDecimal(price)
    }
}