package com.stitchfix.android.takehome.core.dependencyinjection

import com.stitchfix.android.takehome.network.api.APIService
import com.stitchfix.android.takehome.network.api.ApiHelper
import com.stitchfix.android.takehome.network.data.BoxResponse
import javax.inject.Inject

class APIServiceImpl @Inject constructor(private val apiService: APIService) : ApiHelper {
    override suspend fun currentFix(): BoxResponse  = apiService.currentFix()
}