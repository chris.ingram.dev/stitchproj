package com.stitchfix.android.takehome.main.frgments

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

    fun <T> Fragment.viewLifecycle(): ReadWriteProperty<Fragment, T> =
        object: ReadWriteProperty<Fragment, T>, LifecycleObserver {

            // A backing property to hold our value
            private var binding: T? = null

            init {
                // Observe the View Lifecycle of the Fragment
                // * See Gist for full code *
                this@viewLifecycle
                    .viewLifecycleOwnerLiveData
                    .observe(this@viewLifecycle, { owner: LifecycleOwner? ->
                        owner?.lifecycle?.addObserver(this)
                    })
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                // Clear out backing property just before onDestroyView
                binding = null
            }

            override fun getValue(
                thisRef: Fragment,
                property: KProperty<*>
            ): T {
                // Return the backing property if it's set
                return this.binding!!
            }
            override fun setValue(
                thisRef: Fragment,
                property: KProperty<*>,
                value: T
            ) {
                // Set the backing property
                this.binding = value
            }
        }