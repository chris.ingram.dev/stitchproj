package com.stitchfix.android.takehome.core

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    // Add in any needed code here
}