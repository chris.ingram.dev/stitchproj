package com.stitchfix.android.takehome.ui.checkout.holder

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stitchfix.android.takehome.R
import com.stitchfix.android.takehome.databinding.ListItemProductBinding
import com.stitchfix.android.takehome.extensions.load
import com.stitchfix.android.takehome.main.StitchUtils
import com.stitchfix.android.takehome.network.data.Product
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

class ProductHolder(
    private var binding: ListItemProductBinding,
    var onDiscard: OnDiscard,
    var productSelect: OnProductSelectListener,
    var isFinalized: Boolean): RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Product, position:Int, isTrash:Boolean) {
        binding.brand.text = product.brand
        binding.name.text = product.name
        binding.productImage.load(product.image_url)

        binding.price.text = StitchUtils.formatCurrency(product.getPrice())

        if(isFinalized) {
            binding.discard.visibility = View.GONE
        } else {
            binding.discard.visibility = View.VISIBLE
        }

        if(isTrash) {
            binding.discard.setImageDrawable(ContextCompat.getDrawable(binding.root.context, R.drawable.ic_add))
            binding.productImage.colorFilter = ColorMatrixColorFilter(ColorMatrix().apply { setSaturation(0f)})
            binding.root.setBackgroundColor(binding.root.resources.getColor(R.color.disabled, null))

        } else {
            binding.discard.setImageDrawable(ContextCompat.getDrawable(binding.root.context, R.drawable.ic_delete))
            binding.productImage.colorFilter = ColorMatrixColorFilter(ColorMatrix().apply { setSaturation(1f)})
            binding.root.setBackgroundColor(binding.root.resources.getColor(R.color.translucentWhite, null))
        }

        binding.discard.setOnClickListener {
            onDiscard.discard(product, position)
        }

        binding.root.setOnClickListener {
            productSelect.selected(product)
        }
    }

    interface OnDiscard {
        fun discard(product: Product, position: Int)
    }

    interface OnProductSelectListener {
        fun selected(product: Product)
    }

}