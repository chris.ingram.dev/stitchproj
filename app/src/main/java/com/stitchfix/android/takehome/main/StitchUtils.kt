package com.stitchfix.android.takehome.main

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

class StitchUtils {

    companion object {
        fun formatCurrency(bigDecimal: BigDecimal) : String {
            val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
            return format.format(bigDecimal)
        }
    }
}