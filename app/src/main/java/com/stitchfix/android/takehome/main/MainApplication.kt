package com.stitchfix.android.takehome.main

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application() {

    // Place any Application code needed here!

}