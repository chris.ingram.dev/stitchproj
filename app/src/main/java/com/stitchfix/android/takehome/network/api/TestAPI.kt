package com.stitchfix.android.takehome.network.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object TestAPI {

    private const val BASE_URL = "https://fake-mobile-backend.production.stitchfix.com/"

    private val retrofitBuilder: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    // This is the API service to call directly or use DI Provider
    val apiService: APIService = retrofitBuilder.create(APIService::class.java)
}