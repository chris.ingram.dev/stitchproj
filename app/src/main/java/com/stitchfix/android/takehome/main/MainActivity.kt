package com.stitchfix.android.takehome.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.stitchfix.android.takehome.R
import com.stitchfix.android.takehome.core.BaseActivity
import com.stitchfix.android.takehome.databinding.ActivityMainBinding
import com.stitchfix.android.takehome.databinding.ViewStartupLayoutBinding
import com.stitchfix.android.takehome.extensions.load
import com.stitchfix.android.takehome.extensions.loadCircleCrop
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    // View Binding for the Main Activity Layout
    private lateinit var mainBinding: ActivityMainBinding

    @SuppressLint("SetTextI18n", "ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        val viewRoot = mainBinding.root
        setContentView(viewRoot)

    }

}