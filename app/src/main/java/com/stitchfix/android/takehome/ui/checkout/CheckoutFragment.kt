package com.stitchfix.android.takehome.ui.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stitchfix.android.takehome.ProductFragment
import com.stitchfix.android.takehome.R
import com.stitchfix.android.takehome.databinding.CheckoutFragmentBinding
import com.stitchfix.android.takehome.main.frgments.viewLifecycle
import com.stitchfix.android.takehome.network.data.Product
import com.stitchfix.android.takehome.ui.checkout.adapter.ProductAdapter
import com.stitchfix.android.takehome.ui.checkout.holder.ProductHolder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutFragment : Fragment(), ProductHolder.OnProductSelectListener {

    private var binding: CheckoutFragmentBinding by viewLifecycle()
    private val checkoutViewModel: CheckoutViewModel by viewModels()
    private lateinit var productAdapter: ProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CheckoutFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productAdapter = ProductAdapter(this)
        binding.productRecycler.adapter = productAdapter
        binding.productRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.productRecycler.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                RecyclerView.VERTICAL
            )
        )

        checkoutViewModel.currentFix().observe(viewLifecycleOwner, { result ->
            result.success?.let {
                binding.checkoutProgress.visibility = View.GONE
                productAdapter.productList.clear()
                productAdapter.productList.addAll(it.shipment_items)
                productAdapter.notifyDataSetChanged()
            }

            result.error?.let {
                binding.checkoutProgress.visibility = View.GONE
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }

        })

        binding.checkoutButton.setOnClickListener {
            if (!checkoutViewModel.readyForCheckout) {
                removeUnwantedItems()
                productAdapter.setFinalize(true)
                val checkoutFinal =
                    "${getString(R.string.checkout)} : ${checkoutViewModel.totalCost(productAdapter.productList.toList())}"
                binding.checkoutButton.text = checkoutFinal
                binding.checkoutButton.backgroundTintList =
                    ContextCompat.getColorStateList(requireContext(), R.color.colorAccent)
                binding.cancelButton.visibility = View.VISIBLE
            } else {
                Toast.makeText(requireContext(), "You bought it!", Toast.LENGTH_SHORT).show()
            }
            checkoutViewModel.readyForCheckout = true

        }

        binding.cancelButton.setOnClickListener {
            checkoutViewModel.readyForCheckout = false
            productAdapter.setFinalize(false)
            addUnwantedItems()
            binding.checkoutButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            binding.checkoutButton.text = getString(R.string.checkout)
            binding.cancelButton.visibility = View.GONE
        }

    }

    private fun removeUnwantedItems() {
        for (i in productAdapter.deletedProducts.indices) {
            val index = productAdapter.productList.indexOf(productAdapter.deletedProducts[i])
            productAdapter.productList.removeAt(index)
            productAdapter.notifyItemRemoved(index)
        }
    }

    private fun addUnwantedItems() {
        for (i in 0 until productAdapter.selectedItems.size()) {
                productAdapter.productList.add(productAdapter.selectedItems.keyAt(i), productAdapter.deletedProducts[i])
                productAdapter.notifyItemInserted(productAdapter.selectedItems.keyAt(i))
        }
    }

    override fun selected(product: Product) {
        findNavController().navigate(CheckoutFragmentDirections.actionCheckoutFragmentToProductFragment(product.brand, product.name, product.getPrice().toFloat(), product.image_url))
    }

}