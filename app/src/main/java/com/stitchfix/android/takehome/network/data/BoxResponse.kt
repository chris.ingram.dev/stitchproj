package com.stitchfix.android.takehome.network.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BoxResponse(
    val id: Long,
    val shipment_items: List<Product>
)