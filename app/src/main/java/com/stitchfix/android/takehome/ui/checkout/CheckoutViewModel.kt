package com.stitchfix.android.takehome.ui.checkout

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.stitchfix.android.takehome.network.api.CheckoutRepository
import com.stitchfix.android.takehome.network.data.Product
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*


class CheckoutViewModel @ViewModelInject constructor(
    private val checkoutRepository: CheckoutRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel(), LifecycleObserver {

    var readyForCheckout:Boolean = false

    fun currentFix() = liveData {
        val data = checkoutRepository.currentFix()
        emit(data)
    }

    fun totalCost(products: List<Product>): String {
        var totalCost = BigDecimal(0)
        for (i in products.indices) {
            totalCost += products[i].getPrice()
        }
        val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
        return format.format(totalCost)
    }
}