package com.stitchfix.android.takehome.network.api

import com.stitchfix.android.takehome.network.data.BoxResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    //Function to get the API response
    @GET("api/current_fix")
    suspend fun currentFix(): BoxResponse
}