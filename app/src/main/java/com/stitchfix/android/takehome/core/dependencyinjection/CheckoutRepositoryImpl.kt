package com.stitchfix.android.takehome.core.dependencyinjection

import com.stitchfix.android.takehome.network.api.ApiHelper
import com.stitchfix.android.takehome.network.api.CheckoutRepository
import javax.inject.Inject

class CheckoutRepositoryImpl @Inject constructor(apiHelper: ApiHelper) : CheckoutRepository(apiHelper)
