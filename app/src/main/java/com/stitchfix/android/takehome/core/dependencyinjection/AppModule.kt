package com.stitchfix.android.takehome.core.dependencyinjection

import android.app.Application
import com.stitchfix.android.takehome.core.coroutines.AppCoroutineContextProvider
import com.stitchfix.android.takehome.core.coroutines.CoroutineContextProvider
import com.stitchfix.android.takehome.main.MainApplication
import com.stitchfix.android.takehome.network.api.ApiHelper
import com.stitchfix.android.takehome.network.api.TestAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

//Dagger
@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    fun app(application: Application): MainApplication = application as MainApplication

    @Provides
    fun contextProvider(): CoroutineContextProvider = AppCoroutineContextProvider()

    @Provides
    fun provideAPIService() = TestAPI.apiService

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: APIServiceImpl): ApiHelper = apiHelper

}