package com.stitchfix.android.takehome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.stitchfix.android.takehome.databinding.FragmentProductBinding
import com.stitchfix.android.takehome.extensions.load
import com.stitchfix.android.takehome.main.StitchUtils
import com.stitchfix.android.takehome.main.frgments.viewLifecycle

class ProductFragment : Fragment() {

    var binding: FragmentProductBinding by viewLifecycle()
    val args:ProductFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentProductBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.brand.text = args.brand
        binding.name.text = args.name
        binding.price.text = StitchUtils.formatCurrency(args.price.toBigDecimal())

        binding.productImage.load(args.productImage)

    }

}