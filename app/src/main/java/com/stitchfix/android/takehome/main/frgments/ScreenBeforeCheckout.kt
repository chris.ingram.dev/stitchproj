package com.stitchfix.android.takehome.main.frgments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.stitchfix.android.takehome.R
import com.stitchfix.android.takehome.databinding.FragmentScreenBeforeCheckoutBinding
import com.stitchfix.android.takehome.extensions.loadCircleCrop

class ScreenBeforeCheckout : Fragment() {

    var binding: FragmentScreenBeforeCheckoutBinding by viewLifecycle()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentScreenBeforeCheckoutBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set up Basic page
        binding.profileImage.loadCircleCrop(getString(R.string.user_profileimage))
        val username =  getString(R.string.user_firstname) + " " + getString(R.string.user_lastname)
        binding.profileName.text = username
        binding.checkoutButton.setOnClickListener {
            findNavController().navigate(ScreenBeforeCheckoutDirections.actionScreenBeforeCheckoutToCheckoutFragment())
        }
    }

}