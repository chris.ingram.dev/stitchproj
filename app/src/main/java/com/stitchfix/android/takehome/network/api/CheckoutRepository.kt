package com.stitchfix.android.takehome.network.api

import com.stitchfix.android.takehome.network.api.TestAPI.apiService
import com.stitchfix.android.takehome.network.data.BoxResponse
import retrofit2.HttpException
import javax.inject.Inject

open class CheckoutRepository @Inject constructor(var apiHelper: ApiHelper) {

    suspend fun currentFix(): APIResult<BoxResponse> {
        return try {
            APIResult(success = apiHelper.currentFix())
        } catch (e:HttpException) {
            APIResult(error = e.message())
        }
    }
}