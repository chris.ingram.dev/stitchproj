package com.stitchfix.android.takehome

import android.content.Context
import android.content.res.Resources
import android.content.res.Resources.Theme
import android.test.mock.MockContext
import androidx.test.core.app.ApplicationProvider

// Allows test of items that use resources or package information
open class MockActivityContext : MockContext() {

    override fun getTheme(): Theme {
        return applicationContext.theme
    }

    override fun getPackageName(): String {
        return applicationContext.packageName
    }

    override fun getApplicationContext(): Context {
        return ApplicationProvider.getApplicationContext()
    }

    override fun getResources(): Resources {
        return applicationContext.resources
    }
}