package com.stitchfix.android.takehome

import com.stitchfix.android.takehome.main.StitchUtils
import org.junit.Test

import org.junit.Assert.*
import java.math.BigDecimal

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 *
 * Also look to @HiltAndroid for doing test using DI
 */
class ExampleUnitTest {
    @Test
    fun format_currency() {
        assertEquals("$3.29", StitchUtils.formatCurrency(BigDecimal(3.29)))
    }
}