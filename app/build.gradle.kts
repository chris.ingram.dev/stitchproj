plugins {
    id("com.android.application")
    id("dagger.hilt.android.plugin")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android")
    id( "androidx.navigation.safeargs.kotlin")

}

android {
    compileSdkVersion(30)
    buildToolsVersion("30.0.3")

    useLibrary("android.test.runner")
    useLibrary("android.test.base")
    useLibrary("android.test.mock")

    defaultConfig {
        applicationId = "com.stitchfix.android.takehome"
        minSdkVersion(25)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        viewBinding = true
        //compose = true //uncomment to enable for JetpackCompose
    }

    kotlinOptions {
        jvmTarget = "1.8"
        //useIR = true //uncomment to enable for JetpackCompose
    }

    /* uncomment to enable for JetpackCompose
    composeOptions {
        kotlinCompilerVersion = "1.4.21"
        kotlinCompilerExtensionVersion = "1.0.0-alpha10"
    }
     */
}

dependencies {
    //General, JIC you include a local library
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    //Hilt - Dagger2 DI - both indcluded in this
    implementation("com.google.dagger:hilt-android:2.29.1-alpha")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.2.0")
    kapt("com.google.dagger:hilt-android-compiler:2.29.1-alpha")
    kapt ("androidx.hilt:hilt-compiler:1.0.0-alpha02")
    implementation("androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha02")
    //Hilt Test
    testImplementation("com.google.dagger:hilt-android-testing:2.29.1-alpha")
    kaptTest("com.google.dagger:hilt-android-compiler:2.29.1-alpha")
    androidTestImplementation("com.google.dagger:hilt-android-testing:2.29.1-alpha")
    kaptAndroidTest("com.google.dagger:hilt-android-compiler:2.29.1-alpha")

    //AndroidX
    implementation("androidx.core:core-ktx:1.3.2")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.2")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.2")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:2.2.0")
    implementation("androidx.lifecycle:lifecycle-service:2.2.0")
    implementation("androidx.lifecycle:lifecycle-process:2.2.0")
    kapt("androidx.lifecycle:lifecycle-common-java8:2.2.0")
    implementation("androidx.datastore:datastore:1.0.0-alpha06")
    implementation("androidx.datastore:datastore-rxjava2:1.0.0-alpha06")


    //Material / UI / Images
    implementation("com.google.android.material:material:1.2.1")
    implementation("com.github.bumptech.glide:glide:4.11.0")
    kapt("com.github.bumptech.glide:compiler:4.11.0")
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("io.coil-kt:coil:1.1.1")


    //Jetpack Compose - completely optional
    // To enable move Android Build Tools to 4.2 or 7.0 in project build.gradle.kts)
    /*
    implementation("androidx.compose.ui:ui:1.0.0-alpha10")
    implementation("androidx.compose.ui:ui-tooling:1.0.0-alpha10")
    implementation("androidx.compose.foundation:foundation:1.0.0-alpha10")
    implementation("androidx.compose.material:material:1.0.0-alpha10")
    implementation("androidx.compose.material:material-icons-core:1.0.0-alpha10")
    implementation("androidx.compose.material:material-icons-extended:1.0.0-alpha10")
    implementation("androidx.compose.runtime:runtime-rxjava2:1.0.0-alpha10")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.0.0-alpha10")
     */

    //Networking
    implementation("com.squareup.moshi:moshi:1.11.0")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
    kapt ("com.squareup.moshi:moshi-kotlin-codegen:1.9.1")

    //Kotlin and Coroutines (Note : Flow, SharedFlow, and StateFlow)
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.4.21")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.21")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.2")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.2")

    //Testing
    testImplementation("junit:junit:4.13.1")
    androidTestImplementation("androidx.test.ext:junit:1.1.2")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0")
    testImplementation("io.mockk:mockk:1.10.5")
    androidTestImplementation("io.mockk:mockk-android:1.10.5")
    testImplementation("org.robolectric:robolectric:4.5-alpha-1")
    testImplementation("androidx.arch.core:core-testing:2.1.0")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.9.0")


}

kapt { useBuildCache = true }

configurations.all {
    resolutionStrategy.cacheDynamicVersionsFor(0, "seconds")
}