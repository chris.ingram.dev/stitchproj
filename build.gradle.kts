// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        jcenter()
        mavenCentral()
    }
    dependencies {
        //Optional use of Beta or Canary Build Tools / IDE for Hilt transform tests or Jetpack Compose
        classpath("com.android.tools.build:gradle:4.2.0-beta04") //7.0.0-alpha04")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.21")
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.29.1-alpha")
        classpath( "androidx.navigation:navigation-safe-args-gradle-plugin:2.3.2")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven{ url = uri("https://jitpack.io")}
    }
}

tasks.register("clean",Delete::class){
    delete(rootProject.buildDir)
}